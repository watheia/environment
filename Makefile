.PHONY: clean test lint build

SHELL := /bin/bash
PATH := ./node_modules/.bin:$(PATH)

all: node_modules

# for now doesn't have deploy since v3 doesn't have a place for docs and stuff yet
ci:
	$(MAKE) publish

publish: build
	yarn publish

# publish-nightly: build
# 	yarn publish:nightly

build:
	bit build
	# parcel build packages/@react-{spectrum,aria,stately}/*/ packages/@internationalized/*/ --no-minify

# website:
# 	yarn build:docs --public-url /reactspectrum/$$(git rev-parse HEAD)/docs --dist-dir dist/$$(git rev-parse HEAD)/docs

# website-production:
# 	node scripts/buildWebsite.js
# 	cp packages/dev/docs/pages/robots.txt dist/production/docs/robots.txt
# 	node scripts/brotli.js
