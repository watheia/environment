FROM node:lts AS build-env

# Install ssh and bit dependencies
RUN apt-get update
RUN apt-get upgrade -y
RUN apt-get install -y  apt-transport-https gcc make python g++
RUN apt-get install git -y

# Setup bit client

ENV PATH "/root/bin:${PATH}"
COPY . /app
WORKDIR /app

RUN npm install --global @teambit/bvm && \
  bvm install && \
  export PATH="/root/bin:${PATH}" && \
  /root/bin/bit config set analytics_reporting false && \
  /root/bin/bit config set error_reporting false && \
  /root/bin/bit config set no_warnings true && \
  /root/bin/bit init --harmony && \
  /root/bin/bit import


ENTRYPOINT ["/root/bin/bit"]
CMD ["--help"]
